import Head from "next/head";
import Link from "next/link";
import { useEffect, useState } from "react";
import styles from "../styles/Home.module.css";

// export default function Home() {
//     return (
//         <>
//             <Head>
//                 <title>Create Next App</title>
//                 <link rel="icon" href="/favicon.ico" />
//             </Head>
//             <h1>Hello Home</h1>
//             <Link href={"my"}>
//                 <a>go to my</a>
//             </Link>
//         </>
//     );
// }

export default function Home() {
    const [counter, setCounter] = useState(0);
    const [installPrompt, setInstallPrompt] = useState(null);

    const handleInc = () => {
        setCounter(counter + 1);
    };

    const handleDec = () => {
        setCounter(counter - 1);
    };

    useEffect(() => {
        window.addEventListener("beforeinstallprompt", (e) => {
            e.preventDefault();
        console.log("e: ", e);
        setInstallPrompt(e);
        });
    }, []);

    const handleInstal = (e) => {
        console.log("installPrompt: ", installPrompt);
        installPrompt.prompt();
    };

    return (
        <div className={styles.container}>
            <Head>
                <title>Create Next App</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <main className={styles.main}>
                <div>
                    <h1>Hello, this is first screen</h1>
                    <h1>Hello, this is first screen</h1>
                    <h1>Hello, this is first screen</h1>
                    <Link href="my">
                        <a>go to my</a>
                    </Link>

                    <div>{counter}</div>
                    <div>
                        <button onClick={handleInc}>+</button>
                        <button onClick={handleDec}>-</button>
                    </div>
                    <div>
                        <button onClick={handleInstal}>install</button>
                    </div>
                </div>
            </main>

            <footer className={styles.footer}>123</footer>
        </div>
    );
}
